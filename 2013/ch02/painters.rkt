
#lang racket/gui

; by alphard, Feb 2013
;
; Ниже приведены выполненные упражнения по "языку картинок" из SICP.  
; Для примера выводится готовое изображение.
;

(define screenW 640)
(define screenH 640)

(define (make-v x y) (cons x y))
(define (v-x v)(car v))
(define (v-y v)(cdr v))

(define (v-add v1 v2) 
  (make-v (+ (v-x v1) (v-x v2)) (+ (v-y v1) (v-y v2))))
(define (v-sub v1 v2) 
  (make-v (- (v-x v1) (v-x v2)) (- (v-y v1) (v-y v2))))
(define (v-scale s v) (make-v (* (v-x v) s) (* (v-y v) s)))


(define (line s1 s2)(cons s1 s2))
(define (start-segment l)(car l))
(define (end-segment l)(cdr l))

(define (make-frame o e1 e2)(cons o (cons e1 e2)))
(define (origin-frame frame)(car frame))
(define (edge1-frame frame)(car (cdr frame)))
(define (edge2-frame frame)(cdr (cdr frame)))

(define (draw-line dc v1 v2)
  (send dc draw-line (v-x v1) (v-y v1) (v-x v2) (v-y v2)))


(define (frame-coord-map frame)
  (lambda (v)
    (v-add
     (origin-frame frame)
     (v-add (v-scale (v-x v) (edge1-frame frame))
            (v-scale (v-y v) (edge2-frame frame))))))

(define (segments->painter dc segment-list)
  (lambda (frame)
    (for-each
     (lambda (segment)
       (draw-line dc
        ((frame-coord-map frame) (start-segment segment))
        ((frame-coord-map frame) (end-segment segment))))
     segment-list)))


(define (transform-painter painter orgn corner1 corner2)
  (lambda (frame)
    (let ((m (frame-coord-map frame)))
      (let ((new-origin (m orgn)))
        (painter
         (make-frame new-origin
                     (v-sub (m corner1) new-origin)
                     (v-sub (m corner2) new-origin)))))))

(define (flip-vert painter)
  (transform-painter painter
                     (make-v 0.0 1.0)   ; new origin
                     (make-v 1.0 1.0)   ; new end of edge1
                     (make-v 0.0 0.0))) ; new end of edge2
(define (flip-horiz painter)
  (transform-painter painter
                     (make-v 1.0 0.0)   ; new origin
                     (make-v 0.0 0.0)   ; new end of edge1
                     (make-v 1.0 1.0))) ; new end of edge2
(define (shrink-to-upper-right painter)
  (transform-painter painter
                     (make-v 0.5 0.5)
                     (make-v 1.0 0.5)
                     (make-v 0.5 1.0)))
(define (rotate90 painter)
  (transform-painter painter
                     (make-v 1.0 0.0)
                     (make-v 1.0 1.0)
                     (make-v 0.0 0.0)))
(define (identity painter)
  (transform-painter painter
                     (make-v 0.0 0.0)
                     (make-v 1.0 0.0)
                     (make-v 0.0 1.0)))


(define (beside painter1 painter2)
  (let ((split-point (make-v 0.5 0.0)))
    (let ((paint-left
           (transform-painter painter1
                              (make-v 0.0 0.0)
                              split-point
                              (make-v 0.0 1.0)))
          (paint-right
           (transform-painter painter2
                              split-point
                              (make-v 1.0 0.0)
                              (make-v 0.5 1.0))))
      (lambda (frame)
        (paint-left frame)
        (paint-right frame)))))

(define (below painter1 painter2)
  (let ((split-point (make-v 0.0 0.5)))
    (let ((paint-top
           (transform-painter painter1
                              (make-v 0.0 0.0)
                              (make-v 1.0 0.0)
                              split-point))
          (paint-bottom
           (transform-painter painter2
                              split-point
                              (make-v 1.0 0.5)
                              (make-v 0.0 1.0))))
      (lambda (frame)
        (paint-bottom frame)
        (paint-top frame)))))

(define (right-split painter n)
  (if (= n 0)
      painter
      (let ((smaller (right-split painter (- n 1))))
        (beside painter (below smaller smaller)))))
(define (up-split painter n)
  (if (= n 0)
      painter
      (let ((smaller (right-split painter (- n 1))))
        (below painter (beside smaller smaller)))))

(define (corner-split painter n)
  (if (= n 0)
      painter
      (let ((up (up-split painter (- n 1)))
            (right (right-split painter (- n 1))))
        (let ((top-left (beside up up))
              (bottom-right (below right right))
              (corner (corner-split painter (- n 1))))
          (beside (below painter top-left)
                  (below bottom-right corner))))))

(define (square-limit painter n)
  (let ((quarter (corner-split painter n)))
    (let ((half (beside (flip-horiz quarter) quarter)))
      (below (flip-vert half) half))))

; этот painter рисует некую фигуру из прямых линий. Координаты линий задаются в нормированной системе координат
;TODO: нужна функция для построения полигона по набору точек, чтобы не вбивать каждую точку дважды
(define (wave dc) 
  (segments->painter dc 
                     (list (line (make-v 0 0) (make-v 0 1)) 
                           (line (make-v 0 1) (make-v 1 1))
                           (line (make-v 1 1) (make-v 1 0))
                           (line (make-v 1 0) (make-v 0 0))
                           ;
                           (line (make-v 0 0) (make-v 1 1))
                           (line (make-v 1 1) (make-v 0.7 0.15))
                           (line (make-v 0.7 0.15) (make-v 0.55 0.35))
                           (line (make-v 0.55 0.35) (make-v 0 0)))))

; пераметры рисования
(define num-iterations 4)
(define frame-side 500)
;(define origin0 (make-v 70 550))
(define origin0 (make-v
                 (/ (- screenW frame-side) 2)
                 (/ (+ screenH frame-side) 2)))
(define frame0 (make-frame origin0 (make-v frame-side 0) (make-v 0 (- frame-side))))
; вот здесь фактически выводится изображение
(define (painter-test dc)
  ((square-limit (wave dc) num-iterations) frame0))


; главный цикл приложения
(define main-frame (new frame%
                   [label "painters"]
                   [width screenW]
                   [height screenH]))
(new canvas% [parent main-frame]
             [paint-callback
              (lambda (canvas dc)
                (painter-test dc)
                )])
(send main-frame show #t)