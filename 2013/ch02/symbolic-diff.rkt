#lang racket

; alphard, March 2013
; Symbolic differentiation, Ex.2.56-2.57

(define (pow a b) (expt a b))

(define (deriv exp var)
  (cond ((number? exp) 0)
        ((variable? exp)
         (if (same-variable? exp var) 1 0))
        ((sum? exp)
         (make-sum (deriv (addend exp) var)
                   (deriv (augend exp) var)))
        ((product? exp)
         (make-sum
           (make-product (multiplier exp)
                         (deriv (multiplicand exp) var))
           (make-product (deriv (multiplier exp) var)
                         (multiplicand exp))))
        ((exponentiation? exp)
         (let ((expt (make-exponentiation (base exp) (exponent exp))))
           (cond ((number? expt) 0)
                 (else (make-product (exponent exp)
                       (make-exponentiation (base exp) (- (exponent exp) 1)))))))
        (else
         (error "unknown expression type -- DERIV" exp))))

(define (variable? x) (symbol? x))

(define (same-variable? v1 v2)
  (and (variable? v1) (variable? v2) (eq? v1 v2)))

(define (=number? exp num)
  (and (number? exp) (= exp num)))

(define (make-sum a1 a2)
  (cond ((=number? a1 0) a2)
        ((=number? a2 0) a1)
        ((and (number? a1) (number? a2)) (+ a1 a2))
        (else (list '+ a1 a2))))

(define (make-product m1 m2)
  (cond ((or (=number? m1 0) (=number? m2 0)) 0)
        ((=number? m1 1) m2)
        ((=number? m2 1) m1)
        ((and (number? m1) (number? m2)) (* m1 m2))
        (else (list '* m1 m2))))

(define (make-exponentiation base expt)
  (cond ((=number? base 1) 1)
        ((=number? expt 0) 1)
        ((=number? expt 1) base)
        ((and (number? base) (number? expt)) (pow base expt))
        ((variable? expt) (error "unsupported expression type a**x" expt))
        (else (list '** base expt))))
        

(define (sum? x)
  (and (pair? x) (eq? (car x) '+)))
(define (addend s) (cadr s))
(define (augend s) (multi-arg-op s 0 make-sum))
  
(define (product? x)
  (and (pair? x) (eq? (car x) '*)))
(define (multiplier p) (cadr p))
(define (multiplicand p) (multi-arg-op p 1 make-product))


(define (multi-arg-op expr initial fn)
   (define (iter a)
    (cond ((null? a) initial)
          ((pair? a) (fn (car a) (iter (cdr a))))
          (else a)))
  (iter (cddr expr)))

(define (exponentiation? x)
  (and (pair? x) (eq? (car x) '**)))
(define (base x) (cadr x))
(define (exponent x) (caddr x))

; tests of the derivative of the n'th power of x:
; (deriv '(** 2 3) 'x)
; (map (lambda(p)(deriv (list '** 'x p) 'x)) '(0 1 2 3))
;
;
;
; tests of the multiple argument sum and product derivative:
;
; (deriv '(+ (+ x y) (* x y) (** x 2)) 'x)
; (deriv '(* x x x) 'x)
; (deriv '(* x (** x 2)) 'x)


