#lang racket

; Exercise 2.59. Implement the union-set operation for the unordered-list representation of sets.

(define (element-of-set? x set)
  (cond ((null? set) false)
        ((equal? x (car set)) true)
        (else (element-of-set? x (cdr set)))))

(define (adjoin-set x set)
  (if (element-of-set? x set)
      set
      (cons x set)))

(define (intersection-set set1 set2)
  (cond ((or (null? set1) (null? set2)) '())
        ((element-of-set? (car set1) set2)        
         (cons (car set1)
               (intersection-set (cdr set1) set2)))
        (else (intersection-set (cdr set1) set2))))


(define (union-set set1 set2)
  (if (null? set2) set1
      (union-set (adjoin-set (car set2) set1) (cdr set2))))

; Exercise 2.60. (unordered set with duplicates)

; intersection-set, element-of-set? - same as in the previous case.
; 2 operations below are much faster:

(define (union-set-duplicate set1 set2) (append set1 set2))

(define (adjoin-set-duplicate x set) (cons x set))

